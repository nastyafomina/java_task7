import org.junit.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class StreamsTest {
    @Test
    public void testWriteAndReadArrayOfIntegers() throws IOException {
        int[] array = {1, 2, 3, 4, 5};
        int[] arrayExpected = {1, 2, 3, 4, 5};

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Streams.writeArrayOfIntegers(out, array);

            try (ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray())) {
                int[] actual = Streams.readArrayOfIntegers(in, 5);

                assertArrayEquals(arrayExpected, actual);
            }
        }

    }

    @Test
    public void testWriteAndReadArrayOfIntegersBySymbols(){
        int[] array = {11, 12, 13, 14, 15};
        int[] arrayExpected = {11, 12, 13, 14, 15};

        try (StringWriter out = new StringWriter()) {
            Streams.writeArrayOfIntegersBySymbols(out, array, 5);
            String str = out.toString();

            try (StringReader in = new StringReader(str)) {
                int[] actual = Streams.readArrayOfIntegersBySymbols(in, 5);
                assertArrayEquals(arrayExpected, actual);
                assertEquals("11 12 13 14 15 ", str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReadArrayOfIntegersByRandomAccessFile() throws IOException {
        File file = new File("test.txt");
        file.createNewFile();

        try (RandomAccessFile raf = new RandomAccessFile(file, "rw")) {
            for (int i = 0; i < 6; i++) {
                raf.writeInt(i);
            }
            assertArrayEquals(new int[]{0, 1, 2, 3, 4, 5}, Streams.readArrayOfIntegersByRandomAccessFile(file, 0));
            assertArrayEquals(new int[]{2, 3, 4, 5}, Streams.readArrayOfIntegersByRandomAccessFile(file, 8));
            try {
                Streams.readArrayOfIntegersByRandomAccessFile(file, -1);
                fail();
            } catch (IllegalArgumentException e){
                e.getMessage();
            }
        }
        file.delete();
    }

    @Test
    public void testListOfFilesInDirectory() throws IOException {
        File file1 = new File("src/main/java/Person.java");
        File file2 = new File("src/main/java/Flat.java");
        File file3 = new File("src/main/java/House.java");
        File file4 = new File("src/main/java/Streams.java");
        File file5 = new File("src/main/java/HouseService.java");
        File file6 = new File("src/main/java/jsonStringService.java");
        File fileDir = new File("src/main/java");

        assertTrue(file1.exists());
        assertTrue(file2.exists());
        assertTrue(file3.exists());
        assertTrue(file4.exists());
        assertTrue(file5.exists());
        assertTrue(file6.exists());

        List<File> fileList = new ArrayList<>();
        fileList.add(file1);
        fileList.add(file2);
        fileList.add(file3);
        fileList.add(file4);
        fileList.add(file5);
        fileList.add(file6);

        List<File> listExpected = Streams.listOfFilesInDirectory(fileDir, ".java");
        Collections.sort(fileList);
        Collections.sort(listExpected);

        assertEquals(fileList, listExpected);
    }

    @Test
    public void testListFilesInDirectoryWithExtensionResearch() throws IOException {
        Streams streams = new Streams();

        File file1 = new File("src/test/java/HouseServiceTest.java");
        File file2 = new File("src/test/java/JsonStringServiceTest.java");
        File file3 = new File("src/test/java/StreamsTest.java");
        File fileDir = new File("src/test");

        assertTrue(file1.exists());
        assertTrue(file2.exists());
        assertTrue(file3.exists());

        List<File> fileList = new ArrayList<>();
        fileList.add(file1);
        fileList.add(file2);
        fileList.add(file3);

        /* Филиппов А.В. 24.06.2020 Комментарий не удалять.
         Не работает! Не компилируется
         Error:(119, 103) java: incompatible types: java.util.ArrayList<java.lang.Object> cannot be converted to java.util.List<java.io.File>
        */
        List<File> listExpected = streams.listFilesInDirectoryWithExtensionResearch(fileDir, ".java", new ArrayList<File>());
        Collections.sort(fileList);
        Collections.sort(listExpected);

        assertEquals(fileList, listExpected);
    }
}
