import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class JsonStringServiceTest {
    @Test
    public void test() throws IOException {
        String str1 = "{\"firstField\":1, \"secondField\":2, \"thirdField\":3}";
        String str2 = "{\"firstField\":1, \"secondField\":2, \"thirdField\":3}";
        String str3 = "{\"firstField\":2, \"secondField\":3, \"thirdField\":4}";

        assertTrue(JsonStringService.JsonStringsAreEqual(str1, str2));
        assertFalse(JsonStringService.JsonStringsAreEqual(str1, str3));
    }

}