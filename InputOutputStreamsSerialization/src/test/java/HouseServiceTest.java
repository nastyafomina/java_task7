import org.junit.Test;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class HouseServiceTest {
    @Test
    public void testSerializeAndDeserializeHouse() throws IOException {
        Person person1 = new Person("Petrov", "Petr", "Petrovich", 12, 01, 1995);
        Person person2 = new Person("Stepanov", "Stepan", "Stepanovich", 23, 9, 1957);
        Person person3 = new Person("Dmitriev", "Dmitriy", "Dmitrievich", 3, 11, 1968);
        Person person4 = new Person("Alescandrova", "Aleksandra", "Aleksandrovna", 14, 6, 1974);
        Person mainPerson = new Person("Ivanov", "Ivan", "Ivanovich", 5, 10, 1965);

        List<Person> listPerson1 = new ArrayList<>();
        listPerson1.add(person1);
        List<Person> listPerson2 = new ArrayList<>();
        listPerson2.add(person2);
        List<Person> listPerson3 = new ArrayList<>();
        listPerson3.add(person3);
        listPerson3.add(person4);

        Flat flat1 = new Flat(1, 70, listPerson1);
        Flat flat2 = new Flat(2, 60, listPerson2);
        Flat flat3 = new Flat(3, 50, listPerson3);
        List<Flat> flatList = new ArrayList<>();
        flatList.add(flat1);
        flatList.add(flat2);
        flatList.add(flat3);

        House house = new House("123456", "Lesnaya, 291", mainPerson, flatList);

        //serializeHouse, deserializeHouse
        byte[] array;

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            HouseService.serializeHouse(objectOutputStream, house);
            array = byteArrayOutputStream.toByteArray();

            try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(array))) {
                House deserializedHouse = HouseService.deserializeHouse(objectInputStream);
                assertEquals(house, deserializedHouse);
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        //serializeHouseByJackson, deserializeHouseByJackson
        String jsonStr = HouseService.serializeHouseByJackson(house);
        assertEquals(house, HouseService.deserializeHouseByJackson(jsonStr));

        //houseToCSVFile
        HouseService houseService = new HouseService();
        houseService.houseToCSVFile(house);

        File fileCSV = new File("house_123456.csv");
        assertTrue(fileCSV.exists());

        String stringExpected = "Information about the house:\n"+ "Cadastar number;123456\n" + "Address;Lesnaya, 291\n" +
                "Senior housekeeper;Ivanov Ivan Ivanovich\n" + "Information about the flats: \n" +
                "Number;Area;Owners\n" + "1;70.0;Petrov Petr Petrovich, \n" + "2;60.0;Stepanov Stepan Stepanovich, \n" +
                "3;50.0;Dmitriev Dmitriy Dmitrievich, Alescandrova Aleksandra Aleksandrovna, \n";
        StringBuilder actual = new StringBuilder("");
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileCSV), StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null)
                actual.append(line).append('\n');
        }
        assertEquals(stringExpected, actual.toString());

        fileCSV.delete();
    }

}