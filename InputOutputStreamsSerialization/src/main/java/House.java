import java.io.Serializable;
import java.util.*;

public class House implements Serializable {
    private String cadastralNumber;
    private String address;
    private Person mainPerson;
    private List<Flat> flatList;

    public House(){}
    public House(String cadastralNumber, String address, Person mainPerson, List<Flat> flatList){
        this.cadastralNumber = cadastralNumber;
        this.address = address;
        this.mainPerson = mainPerson;
        this.flatList = flatList;
    }

    public String getCadastralNumber() { return cadastralNumber; }
    public String getAddress() { return address; }
    public Person getMainPerson() { return mainPerson; }
    public List<Flat> getFlatList() { return flatList; }

    public void setCadastralNumber(String cadastralNumber) { this.cadastralNumber = cadastralNumber; }
    public void setAddress(String address) { this.address = address; }
    public void setMainPerson(Person mainPerson) { this.mainPerson = mainPerson; }
    public void setFlatList(List<Flat> flatList) { this.flatList = flatList; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(cadastralNumber, house.cadastralNumber) &&
                Objects.equals(address, house.address) &&
                Objects.equals(mainPerson, house.mainPerson) &&
                Objects.equals(flatList, house.flatList);
    }
    @Override
    public int hashCode() {
        return Objects.hash(cadastralNumber, address, mainPerson, flatList);
    }
}
