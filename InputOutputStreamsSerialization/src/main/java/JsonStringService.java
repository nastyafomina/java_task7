import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class JsonStringService {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static boolean JsonStringsAreEqual(String jsonStr1, String jsonStr2) throws IOException {
        return objectMapper.readTree(jsonStr1).equals(objectMapper.readTree(jsonStr2));
    }

}
