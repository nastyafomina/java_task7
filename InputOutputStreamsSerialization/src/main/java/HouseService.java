import java.io.*;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HouseService {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void serializeHouse(ObjectOutputStream objectOutputStream, House house) throws IOException {
        objectOutputStream.writeObject(house);
    }
    public static House deserializeHouse(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException{
        return (House) objectInputStream.readObject();
    }
    public static String serializeHouseByJackson(House house) throws IOException {
        return objectMapper.writeValueAsString(house);
    }
    public static House deserializeHouseByJackson(String jsonStr) throws IOException {
        return objectMapper.readValue(jsonStr, House.class);
    }

    public void houseToCSVFile(House house) throws IOException, NullPointerException {
        if(house == null){ throw new NullPointerException(); }
        File file = new File("house_"+house.getCadastralNumber()+".csv");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("Information about the house:\n");
        fileWriter.write("Cadastar number" + ';' + house.getCadastralNumber() + "\n");
        fileWriter.write("Address" + ';' + house.getAddress() + "\n");
        fileWriter.write("Senior housekeeper" + ';' + house.getMainPerson().toStringName() + "\n");
        fileWriter.write("Information about the flats: \n");
        fileWriter.write("Number" + ';' + "Area" + ';' + "Owners" + "\n");
        String owners;
        for(Flat flat : house.getFlatList()){
            owners = new String("");
            for(Person person : flat.getOwners()){
                owners += person.toStringName();
                owners += ", ";
            }
            fileWriter.write(flat.getNumber() + ";" + flat.getArea() + ";" + owners + "\n");
        }
        fileWriter.flush();
        fileWriter.close();
    }
}
