import java.io.Serializable;
import java.util.*;

public class Person implements Serializable {
    private String surname;
    private String name;
    private String patronymic;
    private int day;
    private int month;
    private int year;
    private String birthday;

    public Person(){}
    public Person(String surname, String name, String patronymic, int day, int month, int year) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public String getName() { return name; }
    public String getSurname() {
        return surname;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public String getBirthday() { return birthday; }
    public int getDay(){ return day; }
    public int getMonth(){ return month; }
    public int getYear(){ return year; }

    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) { this.surname = surname; }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
    public void setAge(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public void setBirthday(String birthday){ this.birthday = birthday; }

    public String toStringName(){
        return surname + " " + name + " " + patronymic;
    }

    @Override
    public String toString(){
        return surname + " " + name + " " + patronymic + ", " + "birthday: " + getBirthday() + "\n";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return day == person.day &&
                month == person.month &&
                year == person.year &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(name, person.name) &&
                Objects.equals(patronymic, person.patronymic);
    }
    @Override
    public int hashCode() {
        return Objects.hash(surname, name, patronymic, day, month, year);
    }
}
