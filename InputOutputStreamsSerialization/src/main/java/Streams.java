import java.io.*;
import java.util.*;
import java.nio.charset.StandardCharsets;

public class Streams {
    public static void writeArrayOfIntegers(OutputStream outputStream, int[] array){
        try (DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) {
            for (int elem : array) {
                dataOutputStream.writeInt(elem);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static int[] readArrayOfIntegers(InputStream inputStream, int size) throws IOException {
        int[] array = new int[size];
        byte[] value = new byte[4];

        for (int i = 0; i < size; i++) {
            if (inputStream.read(value) < 4) {
                throw new IOException();
            }
            array[i] = ((value[0] & 0xff) << 24) + ((value[1] & 0xff) << 16) + ((value[2] & 0xff) << 8) + (value[3] & 0xff);
        }
        return array;
    }
    public static void writeArrayOfIntegersBySymbols(Writer writer, int[] array, int size) throws IOException {
        for (int i = 0; i < size; i++) {
            writer.write(Integer.toString(array[i]));
            writer.write(" ");
        }
    }
    public static int[] readArrayOfIntegersBySymbols(Reader reader, int size) throws IOException {
        char[] chars = new char[12 * size];
        if (reader.read(chars) == -1) { throw new IOException(); }

        String[] strings = new String(chars).split(" ");
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = Integer.parseInt(strings[i]);
        }
        return array;
    }
    public static int[] readArrayOfIntegersByRandomAccessFile(File file, int position) {
        if(position < 0){ throw new IllegalArgumentException(); }
        int[] array = new int[(int) (file.length() / 4 - position / 4)];
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
            randomAccessFile.seek(position);
            for (int i = 0; i < array.length; i++) {
                array[i] = randomAccessFile.readInt();
            }
            return array;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static List<File> listOfFilesInDirectory(File directoryPath, String extension) throws IOException {
        List<File> listOfFileNames = new ArrayList<>();
        File[] files = directoryPath.listFiles();
        if (files == null){
            throw new IOException("The directory is empty");
        }
        for (File file : files ) {
            if (file.isFile() && file.getName().endsWith(extension)) {
                listOfFileNames.add(file);
            }
        }
        return listOfFileNames;
    }
    public List<File> listFilesInDirectoryWithExtensionResearch(
            File directoryPath, String extension, List<File> listOfFileNames) throws IOException {
        File[] files = directoryPath.listFiles();
        if (files == null){
            throw new IOException("The directory is empty");
        }
        for (File file : files) {
            if (file.isDirectory()) {
                listFilesInDirectoryWithExtensionResearch(file, extension, listOfFileNames);
            }
            if (file.isFile() && file.getName().endsWith(extension)) {
                listOfFileNames.add(file);
            }
        }
        return listOfFileNames;
    }
}
